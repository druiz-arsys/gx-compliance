import { Module } from '@nestjs/common'

import { CommonModule } from '../common/common.module'
import { JwtSignatureValidationService } from './service/jwt-signature-validation.service'
import { JwtSignatureService } from './service/jwt-signature.service'
import { JwtToVpService } from './service/jwt-to-vp.service'

@Module({
  providers: [CommonModule, JwtSignatureService, JwtSignatureValidationService, JwtToVpService],
  exports: [JwtSignatureService, JwtSignatureValidationService, JwtToVpService]
})
export class JwtModule {}

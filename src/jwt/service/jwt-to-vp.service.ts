import { Injectable } from '@nestjs/common'

import { CredentialSubjectDto, VerifiableCredentialDto, VerifiablePresentationDto } from '../../common/dto'

@Injectable()
export class JwtToVpService {
  public JWTVcsToVP(VCs: any[]): VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>> {
    return {
      '@context': ['https://www.w3.org/ns/credentials/v2'],
      '@type': ['VerifiablePresentation'],
      verifiableCredential: VCs
    }
  }
}

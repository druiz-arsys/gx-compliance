import canonicalize from 'canonicalize'

import * as packageJSON from '../../../package.json'
import { NORMALIZATION_ALG, RULES_VERSION } from '../constants'
import { CredentialSubjectDto, VerifiableCredentialDto } from '../dto'
import { ComplianceEvidenceDTO } from '../dto/compliant-credential-subject.dto'
import { HashingUtils } from '../utils/hashing.utils'
import { VerifiableCredentialUtils } from '../utils/verifiable-credential.utils'

export abstract class CompliantCredentialSubjectMapper {
  abstract type(): string

  public map(verifiableCredential: VerifiableCredentialDto<CredentialSubjectDto>): ComplianceEvidenceDTO {
    const hash: string = HashingUtils.sha256(canonicalize(verifiableCredential))

    return {
      id: verifiableCredential.id ?? verifiableCredential['@id'],
      type: this.type(),
      'gx:integrity': `sha256-${hash}`,
      'gx:integrityNormalization': NORMALIZATION_ALG,
      'gx:engineVersion': packageJSON.version,
      'gx:rulesVersion': RULES_VERSION,
      'gx:originalType': VerifiableCredentialUtils.extractGxTypes(verifiableCredential).join(',')
    }
  }
}

import { HttpModule } from '@nestjs/axios'
import { Test, TestingModule } from '@nestjs/testing'

import * as process from 'process'
import { DatasetCore } from 'rdf-js'

// Fixtures
import { ParticipantVP, ServiceOfferingVP, ServiceOfferingVPProvidedByAbsent, ServiceOfferingVPStructureInvalid } from '../../tests/fixtures'
import { CommonModule } from '../common.module'
import { ShaclService } from './shacl.service'

export const expectedErrorResult = expect.objectContaining({
  conforms: false,
  results: expect.arrayContaining([expect.any(String)])
})

export const expectedValidResult = expect.objectContaining({
  conforms: true
})

describe('ShaclService', () => {
  let shaclService: ShaclService

  const expectedDatasetObject: DatasetCore = {
    size: expect.any(Number),
    has: expect.any(Function),
    delete: expect.any(Function),
    add: expect.any(Function),
    match: expect.any(Function),
    [Symbol.iterator]: expect.any(Object)
  }

  const participantSDRaw = JSON.stringify(ParticipantVP)

  process.env.WEB_DOCUMENT_LOADER = 'true'

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [CommonModule, HttpModule]
    }).compile()
    shaclService = moduleFixture.get<ShaclService>(ShaclService)
  })

  describe('SHACL dataset transformation of raw data', () => {
    it('transforms a dataset correctly from turtle input', async () => {
      const dataset = await shaclService.loadShapesFromRegistryByType('trustframework')
      expectDatasetKeysToExist(dataset)
    })
    it('transforms a dataset correctly from JsonLD input', async () => {
      const dataset = await shaclService.loadFromJSONLDWithQuads(JSON.parse(participantSDRaw))
      expectDatasetKeysToExist(dataset)
    })

    it('transforms a dataset correctly from an url with turtle input', async () => {
      const datasetParticipant = await shaclService.loadShapesFromRegistryByType('trustframework')
      const datasetServiceOffering = await shaclService.loadShapesFromRegistryByType('trustframework')

      expectDatasetKeysToExist(datasetParticipant)
      expectDatasetKeysToExist(datasetServiceOffering)
    })
    it('should throw an error when searching for a non uploaded shape', async () => {
      try {
        await shaclService.loadShapesFromRegistryByType('test')
        fail()
      } catch (e) {
        expect(e.status).toEqual(409)
      }
    })
  })
  describe('SHACL Shape Validation of a Self Descriptions', () => {
    it('returns true for a Self Description using the correct shape', async () => {
      const sdDataset = await shaclService.loadFromJSONLDWithQuads(JSON.parse(participantSDRaw))

      const validationResult = await shaclService.validate(await getShaclShape(), sdDataset)

      expect(validationResult).toEqual(expectedValidResult)
    })
  })

  describe('SHACL Shape Validation of a ServiceOffering', () => {
    it('returns true for a Serviceoffering using the correct shape', async () => {
      const serviceOffering = await shaclService.loadFromJSONLDWithQuads(ServiceOfferingVP)
      const validationResult = await shaclService.validate(await getShaclShape(), serviceOffering)

      expect(validationResult).toEqual(expectedValidResult)
    })
    it('returns false ServiceOffering without proper providedBy', async () => {
      const serviceOffering = await shaclService.loadFromJSONLDWithQuads(ServiceOfferingVPProvidedByAbsent)
      const validationResult = await shaclService.validate(await getShaclShape(), serviceOffering)

      expect(validationResult).toEqual(expectedErrorResult)
    })
    it('returns false ServiceOffering without correct shape', async () => {
      const serviceOffering = await shaclService.loadFromJSONLDWithQuads(ServiceOfferingVPStructureInvalid)
      const validationResultFaulty = await shaclService.validate(await getShaclShape(), serviceOffering)

      expect(validationResultFaulty).toEqual(expectedErrorResult)
    })
  })

  async function getShaclShape() {
    return await shaclService.loadShapesFromRegistryByType('trustframework')
  }

  function expectDatasetKeysToExist(dataset: any) {
    const keys = Object.keys(expectedDatasetObject)
    for (const key of keys) {
      expect(dataset[key]).toBeDefined()
    }
  }
})

import { ApiProperty } from '@nestjs/swagger'

export class ValidationResult {
  @ApiProperty({
    description: 'The data conforms with the given rules or shape'
  })
  public conforms: boolean

  @ApiProperty({
    type: [String],
    description: 'Error messages'
  })
  public results: string[]

  constructor() {
    this.conforms = true
    this.results = []
  }
}

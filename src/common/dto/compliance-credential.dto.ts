import { CredentialSubjectDto } from './credential-meta.dto'

export class ComplianceCredentialDto extends CredentialSubjectDto {}
